package net.stuntguy3000;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class GodmodeTeleport extends JavaPlugin implements Listener{
	
	public enum messages {
		NoDamageGodmode,
		GodmodeActivated,
		GodmodeDisabled,
		NoExtend,
		GodmodeWorldDisabled
	}
	
	public List<String> IgnoredWorlds = new ArrayList<String>();
	public List<String> HaveGodmode = new ArrayList<String>();
	public List<messages> EnabledMessages = new ArrayList<messages>();
	public int time = 5;
	
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		
		IgnoredWorlds = getConfig().getStringList("IgnoredWorlds");
		time = getConfig().getInt("GodmodeTime");
		
		if (getConfig().getBoolean("Messages.NoDamageInGodmode"))
			EnabledMessages.add(messages.GodmodeActivated);
		
		if (getConfig().getBoolean("Messages.GodmodeActive"))
			EnabledMessages.add(messages.GodmodeActivated);
		
		if (getConfig().getBoolean("Messages.GodmodeDisabled"))
			EnabledMessages.add(messages.GodmodeDisabled);
		
		if (getConfig().getBoolean("Messages.NoExtendOnTeleport"))
			EnabledMessages.add(messages.NoExtend);
		
		if (getConfig().getBoolean("Messages.GodmodeWorldDisabled"))
			EnabledMessages.add(messages.GodmodeWorldDisabled);
	}
	
	public void onDisable() {
		Bukkit.getScheduler().cancelTasks(this);
	}
	
	@EventHandler
	private void EntityDamageEvent (EntityDamageEvent event)
	{
		if (event.getEntity() instanceof Player)
		{
			Player p = (Player) event.getEntity();
			
			if (HaveGodmode.contains(p.getName()) && !IgnoredWorlds.contains(p.getWorld().getName()))
				event.setCancelled(true);
		}
	}
	
	@EventHandler
	private void EntityDamageByEntityEvent (EntityDamageByEntityEvent event)
	{
		if (event.getEntity() instanceof Player)
		{
			Player p = (Player) event.getEntity();
			
			if (HaveGodmode.contains(p.getName()) && !IgnoredWorlds.contains(p.getWorld().getName()))
			{
				event.setCancelled(true);
				return;
			}
		
			if (event.getDamager() instanceof Player)
			{
				if (HaveGodmode.contains(((Player) event.getDamager()).getName()) && !IgnoredWorlds.contains(event.getDamager().getWorld().getName()))
				{
					event.setCancelled(true);
					
					Player d = (Player) event.getDamager();
					sendMessage(d, "&cYou cannot damage people while you have godmode!", messages.NoDamageGodmode);
					return;
				}
			}
		}
	}
	
	@EventHandler
	private void PlayerTeleportEvent (PlayerTeleportEvent event)
	{
		if (event.getPlayer().hasPermission("GodmodeTeleport.use") || event.getCause() == TeleportCause.PLUGIN)
		{
			giveGodemode(event.getPlayer());
		}
	}

	private void giveGodemode(final Player p)
	{
		if (p.hasPermission("GodmodeTeleport.use"))
		{
			if (!HaveGodmode.contains(p.getName()))
			{
				sendMessage(p, "&aGodemode activated for &2" + time + "&a seconds.", messages.GodmodeActivated);
				HaveGodmode.add(p.getName());
				
				new BukkitRunnable() {
		    		public void run() {
		    			sendMessage(p, "&cGodemode disabled.", messages.GodmodeDisabled);
		    			HaveGodmode.remove(p.getName());
		    		}
				}.runTaskLater(this, time * 20);
				
				if (IgnoredWorlds.contains(p.getWorld().getName()))
				{
					sendMessage(p, "&cYou still have godmode, however it is disabled in this world.", messages.GodmodeWorldDisabled);
				}
			} else {
				sendMessage(p, "&cYour godmode has not been extended because you already godmode.", messages.NoExtend);
			}
		}
	}
	
	public void sendMessage(Player p, String input, messages msg) {
		String message = ChatColor.translateAlternateColorCodes('&', input);
		
		if (EnabledMessages.contains(msg))
		{
			p.sendMessage(message);
		}
	}
	
}
